package com.example.annotation;

import java.lang.annotation.*;

/**
 * created by Morgan on 2019/4/10
 */
@Documented
@Target({ElementType.TYPE,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface AnnotationDemo {
    String value() default "annotation demo";
}
