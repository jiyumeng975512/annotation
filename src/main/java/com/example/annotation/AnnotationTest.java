package com.example.annotation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * created by Morgan on 2019/4/10
 */
//使用自己自定义的注解
@AnnotationDemo
@ResponseBody
public class AnnotationTest {

    //通过@SuppressWarning 可以去除方法deprecation警告
    @SuppressWarnings(value = "deprecation")
    public static void main(String[] args) {
        System.runFinalizersOnExit(true);
        if (AnnotationTest.class.isAnnotationPresent(AnnotationDemo.class)) {
            //使用反射解析注解
            AnnotationDemo annotationDemo = (AnnotationDemo) AnnotationTest.class.getAnnotation(AnnotationDemo.class);
            System.out.println(annotationDemo.value());
        }

    }
    //当方法被@Deprecatd注释后，调用的地方会对方法中间划根横线，表示该方法已经废弃
    @Deprecated
    public void testAnnotation() {
        System.out.println("test annotation");
    }
    //@Override表明重写父类的方法
    @Override
    public String toString(){
        return "override tostring";
    }
}
